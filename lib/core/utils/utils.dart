import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'package:aes_cryptor/core/data/contact.dart';
import 'package:aes_cryptor/core/data/data_split.dart';
import 'package:aes_cryptor/core/data/file_data_split.dart';
import 'package:cryptography/cryptography.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_pw_validator/Resource/Strings.dart';
import 'package:get/get.dart';
import 'package:collection/collection.dart';

enum EncryptionMethod {
  AES_CBC_PKCS7_HMAC_SHA256,
}


class Utils {
  Utils._();

  //general interface for clearText <-> (iv|cipherText|HMAC) as Uint8Lists
  //data can be clearText (iv|cipherText|HMAC)
  static Future<Uint8List?> crypt(Uint8List key, EncryptionMethod encryptionMethod, Uint8List data, bool modeIsEncrypt, {bool runInParallel = true}) async{
    if(kIsWeb || !runInParallel){
      //no computing in Isolate
      if(modeIsEncrypt){
        return _encrypt(key,encryptionMethod,data);
      }else{
        return _decrypt(key,encryptionMethod,data);
      }
    }else{
      //compute in Isolate
      if(modeIsEncrypt){
        return await compute(_encryptIsolate, [key,encryptionMethod,data]);
      }else{
        return await compute(_decryptIsolate,[key,encryptionMethod,data]);
      }
    }
  }


  static Future<Uint8List?> _encryptIsolate(List<Object> arguments) async{
    assert(arguments[0] is Uint8List);
    assert(arguments[1] is EncryptionMethod);
    assert(arguments[2] is Uint8List);
    final Uint8List key = arguments[0] as Uint8List;
    final EncryptionMethod em = arguments[1] as EncryptionMethod;
    final Uint8List payload = arguments[2] as Uint8List;
    return await _encrypt(key, em, payload);
  }

  static Future<Uint8List?> _decryptIsolate(List<Object> arguments) async{
    assert(arguments[0] is Uint8List);
    assert(arguments[1] is EncryptionMethod);
    assert(arguments[2] is Uint8List);
    final Uint8List key = arguments[0] as Uint8List;
    final EncryptionMethod em = arguments[1] as EncryptionMethod;
    final Uint8List payload = arguments[2] as Uint8List;
    return await _decrypt(key, em, payload);
  }


  //returns iv|cipherText|HMAC as Uint8List
  //will return null if encryption failed and output a message in a snackbar
  static Future<Uint8List?> _encrypt(Uint8List key, EncryptionMethod encryptionMethod, Uint8List plaintext) async {
    if (key.isEmpty || plaintext.isEmpty)
      throw Exception('Unsupported input data for encryption method. This should not happen. Key: $key, data: $plaintext');

    switch (encryptionMethod) {
      case EncryptionMethod.AES_CBC_PKCS7_HMAC_SHA256:
        //initialize paddedBlockCipher
        // AES-CBC with 128 bit keys and HMAC-SHA256 authentication.
        final algorithm = AesCbc.with256bits(macAlgorithm: Hmac.sha256());
        final secretKey = SecretKey(key);
        final nonce = algorithm.newNonce();

        final secretBox = await algorithm.encrypt(
            plaintext,
            secretKey: secretKey,
            nonce: nonce
          );
        return secretBox.concatenation();

      //other cases can be added here
      //case EncryptionMethod.BLOWFISH...
      default:
        Get.snackbar('error_14_title'.tr, 'error_14_msg'.tr);
        return null;
    }
  }

  //returns clearText as Uint8List
  //will return null if encryption failed or HMAC incorrect and output a message in a snackbar
  static Future<Uint8List?> _decrypt(Uint8List key, EncryptionMethod encryptionMethod, Uint8List ciphertext) async {
    if (key.isEmpty || ciphertext.isEmpty)
      throw Exception('Unsupported input data for encryption method. This should not happen. Key: $key, data: $ciphertext');

    switch (encryptionMethod) {
      case EncryptionMethod.AES_CBC_PKCS7_HMAC_SHA256:
      //initialize paddedBlockCipher
      // AES-CBC with 128 bit keys and HMAC-SHA256 authentication.
        final algorithm = AesCbc.with256bits(macAlgorithm: Hmac.sha256());
        final secretKey = SecretKey(key);
      final List<int> decrypted;
        try{
          final secretBox = SecretBox.fromConcatenation(ciphertext, nonceLength: algorithm.nonceLength, macLength: algorithm.macAlgorithm.macLength);
          await secretBox.checkMac(macAlgorithm: algorithm.macAlgorithm, secretKey: secretKey, aad: []);
          decrypted = await algorithm.decrypt(secretBox, secretKey: SecretKey(key));
        }on SecretBoxAuthenticationError{
          Get.snackbar('error_23_title'.tr, 'error_23_msg'.tr);
          return null;
        }on ArgumentError {
          Get.snackbar('error_20_title'.tr, 'error_20_msg'.tr);
          return null;
        }catch (e){
          //this should not happen
          e.printError();
          Get.snackbar('error_21_title'.tr, 'error_21_msg'.tr);
          return null;
        }
        return Uint8List.fromList(decrypted);

    //other cases can be added here
    //case EncryptionMethod.BLOWFISH...
      default:
        Get.snackbar('error_14_title'.tr, 'error_14_msg'.tr);
        return null;
    }
  }

  //Validates the general part of the header, aka magicNumber, headerVersionNumber, encryptionMethod and iv
  //will return null if validation failed
  //will return a DataSplit-Object otherwise
  static Future<DataSplit?> validateAndDecryptMessage(Uint8List data, Contact? contact, bool expectedTargetIsFile) async {
    if (contact == null) {
      Get.snackbar('error_5_title'.tr, 'error_5_msg'.tr);
      return null;
    }
    if (data.length < 40) {
      Get.snackbar('error_7_title'.tr, 'error_7_msg'.tr);
      return null;
    }

    //check bigHMAC first
    final expctedBigMAC = data.sublist(data.length-32);
    final actualBigMAC = await getHMAC(data.sublist(0,data.length-32), contact.password);
    if(!ListEquality().equals(expctedBigMAC,actualBigMAC)){
      Get.snackbar('error_23_title'.tr, 'error_23_msg'.tr);
      return null;
    }

    //subtract bigHMAC from the rest of the data
    data = data.sublist(0,data.length-32);

    final Uint8List magic = data.sublist(0, 10);
    final int headerVersionNumber = data.sublist(10, 12).buffer.asUint16List()[0].toInt();
    final int encryptionMethod = data.sublist(12, 14).buffer.asUint16List()[0].toInt();
    final int isFileFlag = data.elementAt(14);


    //IMPORTANT NOTE: Due to BigHMAC, we do not have to check all of the upcoming flags and bytes to check the data integrity
    //However, bigHMAC was implemented at the end of the project, so it was still left in for debugging purposes
    //This is up to change in later versions

    //check MagicNumber
    assert(magic.length == 10);
    if (utf8.decode(magic) != 'SECENCRYPT') {
      Get.snackbar('error_12_title'.tr, 'error_12_msg'.tr);
      return null;
    }
    //check header version number
    if (headerVersionNumber != 0) {
      Get.snackbar('error_13_title'.tr, 'error_13_msg'.tr);
      return null;
    }
    //check EncryptionMethod
    if (encryptionMethod >= EncryptionMethod.values.length) {
      Get.snackbar('error_14_title'.tr, 'error_14_msg'.tr);
      return null;
    }
    //check isFileFlag
    if (isFileFlag != 0 && isFileFlag != 1) {
      Get.snackbar('error_15_title'.tr, 'error_15_msg'.tr);
      return null;
    }

    final bool actualTargetIsFile = isFileFlag == 1;
    if(actualTargetIsFile != expectedTargetIsFile) {
      if(actualTargetIsFile) {
        Get.snackbar('error_18_title'.tr, 'error_18_msg'.tr);
        return null;
      }else {
        Get.snackbar('error_19_title'.tr, 'error_19_msg'.tr);
      }
    }

    late final String fileNameAsString;
    int messagePayloadLengthStartIndex = 15;

    //only if target is file
    if(actualTargetIsFile) {
      final int fileNameIsEncryptedFlag = data.elementAt(15);
      final int fileNameLength = data.sublist(16, 18).buffer.asUint16List()[0];

      //check fileNameIsEncryptedFlag
      if (fileNameIsEncryptedFlag != 0 && fileNameIsEncryptedFlag != 1) {
        Get.snackbar('error_8_title'.tr, 'error_8_msg'.tr);
        return null;
      }
      //check file name length > 0
      if (fileNameLength == 0) {
        Get.snackbar('error_9_title'.tr, 'error_9_msg'.tr);
        return null;
      }
      if(data.length < 18 + fileNameLength) {
        Get.snackbar('error_7_title'.tr, 'error_7_msg'.tr);
        return null;
      }

      Uint8List? fileName = data.sublist(18, 18 + fileNameLength);

      if (fileNameIsEncryptedFlag == 1) {
        fileName = await crypt(await pbkdf2(contact.password), EncryptionMethod.values[encryptionMethod], fileName, false, runInParallel: false);
        if (fileName == null) return null;
      }

      try{
        fileNameAsString = utf8.decode(fileName);
      }catch (e){
        Get.snackbar('error_22_title'.tr, 'error_22_msg'.tr);
        return null;
      }

      messagePayloadLengthStartIndex = 18 + fileNameLength;
    }

    if(data.length < messagePayloadLengthStartIndex + 4 + 1) {
      Get.snackbar('error_7_title'.tr, 'error_7_msg'.tr);
      return null;
    }

    final Uint8List payloadLengthList = data.sublist(messagePayloadLengthStartIndex, messagePayloadLengthStartIndex + 4);
    assert(payloadLengthList.length == 4);
    final int messagePayloadLength = payloadLengthList.buffer.asUint32List()[0].toInt();
    final Uint8List messagePayload = data.sublist(messagePayloadLengthStartIndex + 4);

    if (messagePayload.lengthInBytes != messagePayloadLength) {
      Get.snackbar('error_10_title'.tr, 'error_10_msg'.tr);
      return null;
    }

    final Uint8List? message = await crypt(await pbkdf2(contact.password), EncryptionMethod.values[encryptionMethod], messagePayload, false, runInParallel: expectedTargetIsFile);
    if(message == null) return null;


    return actualTargetIsFile ? FileDataSplit(fileNameAsString, message) : DataSplit(message);
  }

  static Future<Uint8List> pbkdf2(String password, {Uint8List? iv}) async {
    //give salt a default value if salt is not needed
    iv = iv ?? Uint8List.fromList(utf8.encode('0000000000000000'));

    final pbkdf2 = Pbkdf2(
      macAlgorithm: Hmac.sha256(),
      iterations: 10000,
      bits: 256,
    );

    // Password we want to hash
    final secretKey = SecretKey(utf8.encode(password));

    // Calculate a hash that can be stored in the database
    final newSecretKey = await pbkdf2.deriveKey(
      secretKey: secretKey,
      nonce: iv,
    );
    Uint8List result = Uint8List.fromList(await newSecretKey.extractBytes());

    return result;
  }

  static Future<Uint8List> getHash(Uint8List key) async => Uint8List.fromList((await Sha256().hash(key)).bytes);

  static Future<Uint8List> getHMAC(Uint8List data, String password) async {
    final hmac = Hmac.sha256();
    final mac = await hmac.calculateMac(
      data,
      secretKey: SecretKey(utf8.encode(password)),
    );
    return Uint8List.fromList(mac.bytes);
  }

  static Uint8List getSecureUint8List(int length) {
    final srng = Random.secure();
    Uint8List salt = new Uint8List(length);
    for (int i = 0; i < salt.length; i++) {
      salt[i] = srng.nextInt(255);
    }
    return salt;
  }

  //This password validator is by no means perfect
  //However implementing an actual "good" one would have taken too much time
  static String? validatePw(String? text) {
    if (text == null || text.isEmpty) {
      return 'utils_password_validate1'.tr;
    } else if (text.length < 10) {
      return 'utils_password_validate2'.tr;
    }else if(!text.contains(RegExp(r'[A-Z]'))){
      return 'utils_password_validate3'.tr;
    }else if(!text.contains(RegExp(r'[a-z]'))){
      return 'utils_password_validate4'.tr;
    }else if(!text.contains(RegExp(r'[0-9]'))){
      return 'utils_password_validate5'.tr;
    }else if(!text.contains(RegExp(r'[@!"§$%&\/()=?*+~#_\-:.;,><|{\[\]}\\'+"'" + ']'))){
      return 'utils_password_validate6'.tr;
    }else if(text.contains(RegExp(r'[^A-Za-z0-9@!"§$%&\/()=?*+~#_\-:.;,><|{\[\]}\\'+"'"+ ']'))){
      return 'utils_password_validate7'.tr;
    }
    return null;
  }

  static String? validateNonEmpty(String? name) {
    if (name == null || name.isEmpty) {
      return 'utils_field_nonempty'.tr;
    }
    return null;
  }

  static String generateSecurePassword(int length) {
    final characterSets =[ r'ABCDEFGHIJKLMNOPQRSTUVWXYZ', r'abcdefghijklmnopqrstuvwxyz', r'0123456789', r'!$%&/()=?+*#-_'];
    if(length < characterSets.length) throw ArgumentError();
    StringBuffer result = StringBuffer();
    final rng = Random.secure();

    for(int i = 0; i < length; i++){
      final characterSet = characterSets[rng.nextInt(characterSets.length)];
      final character = characterSet[rng.nextInt(characterSet.length)];
      result.write(character);
    }

    final resultString = result.toString();
    return _checkCharacterSets(characterSets, resultString) ? resultString : generateSecurePassword(length);
  }

  static void copyToClipboard(TextEditingController controller) => Clipboard.setData(ClipboardData(text: controller.text));

  static Future<void> pasteFromClipboard(TextEditingController controller) async {
    ClipboardData? data = await Clipboard.getData('text/plain');
    if(data != null){
      final clipboardText = data.text;
      if(clipboardText != null){
        controller.text = clipboardText;
      }
    }
    controller.selection = TextSelection.collapsed(offset: controller.text.length);
  }


  static bool _checkCharacterSets(List<String> characterSets, String check){
    for(final characterSet in characterSets){
      for(int i = 0; i < characterSet.length; i++){
        String character = characterSet[i];
        if(check.contains(character)){
          break;
        }else{
          if(i == characterSet.length-1){
            return false;
          }
        }
      }
    }
    return true;
  }

}


class GlobalStrings implements FlutterPwValidatorStrings {


  @override
  final String atLeast = 'utils_password_validate1'.tr;
  @override
  final String normalLetters = 'utils_password_validate2'.tr;
  @override
  final String uppercaseLetters = 'utils_password_validate3'.tr;
  @override
  final String numericCharacters = 'utils_password_validate4'.tr;
  @override
  final String specialCharacters = 'utils_password_validate5'.tr;

}




