import 'package:flutter/material.dart';

class Responsive extends StatelessWidget {
  final Widget smallDevice;
  //final Widget mediumDevice;
  final Widget bigDevice;

  const Responsive({
    required this.smallDevice,
    //required this.tablet,
    required this.bigDevice,
  });


  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      // If our width is more than 950 then we consider it a bigDevice
      builder: (context, constraints) {
        if (MediaQuery.of(context).size.width > 950) {
          return bigDevice;
        }
        // If width it less then 1100 and more then 650 we consider it as mediumDevice
        //else if (constraints.maxWidth >= 650) {
          //return mediumDevice;
        //}
        // Or less then that we called it smallDevice
        else {
          return smallDevice;
        }
      },
    );
  }
}