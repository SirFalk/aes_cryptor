import 'package:aes_cryptor/core/ui/status/status_controller.dart';
import 'package:aes_cryptor/core/ui/default/default_error_widget.dart';
import 'package:aes_cryptor/core/ui/default/default_loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StatusWidget extends StatelessWidget {
  late final Rx<Status> status;
  final Widget onSuccess;
  final Widget? onLoading;
  final Widget? onError;

  StatusWidget(StatusController controller, {required this.onSuccess, this.onLoading, this.onError}){
    this.status = controller.status;
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => _switchStatus());
  }

  Widget _switchStatus(){
    switch (status.value) {
      case Status.success:
        return onSuccess;
      case Status.loading:
        return onLoading ?? DefaultLoadingWidget();
      case Status.error:
        return onError ?? DefaultErrorWidget();
    }
  }
}
