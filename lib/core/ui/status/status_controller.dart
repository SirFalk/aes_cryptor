import 'package:get/get.dart';

enum Status{
  loading,
  success,
  error,

}

abstract class StatusController extends GetxController{
  Rx<Status> status = Status.success.obs;

}