import 'package:flutter/material.dart';

class DefaultButton extends StatelessWidget {
  final String text;
  final VoidCallback? onPressed;


  DefaultButton({required this.text, required this.onPressed});

   @override
  Widget build(BuildContext context) {
     return Padding(
       padding: const EdgeInsets.symmetric(vertical: 8.0),
       child: ElevatedButton(
         style: ButtonStyle(
           minimumSize: MaterialStateProperty.all(Size.fromHeight(50.0)),
           textStyle: MaterialStateProperty.all(TextStyle(fontSize: 16)),
         ),
         onPressed: onPressed,
         child: Text(text),
       ),
     );

  }
}
