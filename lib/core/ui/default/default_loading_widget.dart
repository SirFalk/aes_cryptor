import 'package:flutter/material.dart';

class DefaultLoadingWidget extends StatelessWidget {
   @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(child: CircularProgressIndicator()));
  }
}
