import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DefaultErrorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(child: Text('default_error_text'.tr)));
  }
}
