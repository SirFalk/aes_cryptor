import 'package:flutter/material.dart';


class DefaultAppBar extends StatelessWidget with PreferredSizeWidget{

  final String title;
  DefaultAppBar(this.title);

  @override
  PreferredSizeWidget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      centerTitle: true,
    );
  }

  @override
  Size get preferredSize => AppBar().preferredSize;
}


