import 'dart:math';

import 'package:flutter/material.dart';

class DefaultConstraints extends StatelessWidget {
  final Widget child;
  final double amplifier;

  DefaultConstraints({required this.child, this.amplifier = 1});

  @override
  Widget build(BuildContext context) {
    double maxWidth = amplifier*(400 + (MediaQuery.of(context).size.width - 400)/8);
    //screen height - appbar height - bottom nav bar height
    double maxHeight = min(MediaQuery.of(context).size.height - 80 - 56, 700);
    return GestureDetector(
      child: Center(
        child: ConstrainedBox(
          constraints: BoxConstraints(maxWidth: maxWidth,maxHeight: maxHeight),
          child: child,
        ),
      ),
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),

    );
  }
}
