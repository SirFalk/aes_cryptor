import 'dart:typed_data';

import 'package:aes_cryptor/core/utils/utils.dart';

import 'data_split.dart';

class FileDataSplit extends DataSplit{
  String fileName;

  FileDataSplit(this.fileName, Uint8List payload) : super(payload);

}