import 'dart:ui';

import 'package:hive/hive.dart';

part 'contact.g.dart';

@HiveType(typeId: 0)
class Contact{
  @HiveField(0)
  String name;

  @HiveField(1)
  String password;

  Contact(this.name, this.password);



  @override
  operator ==(o) =>
      o is Contact &&
          o.name == name &&
          o.password == password;

  @override
  int get hashCode => hashValues(name, password);
}