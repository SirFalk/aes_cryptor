import 'package:aes_cryptor/core/ui/default/default_constaints.dart';
import 'package:aes_cryptor/core/ui/status/status_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'login_controller.dart';


class LoginView extends GetView<LoginController> {
  final LoginController controller = Get.put(LoginController());


  @override
  Widget build(BuildContext context) {
    return StatusWidget(controller, onSuccess: _page());
  }

  Widget _page() {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Get.theme.iconTheme.color,
        ),
      ),
      body: DefaultConstraints(
        child: Padding(

          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: controller.formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Text('login_notice'.tr),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    controller: controller.passwordController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'master_password_label'.tr,
                    ),
                    validator: controller.validatePw,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      minimumSize: MaterialStateProperty.all(Size.fromHeight(50.0)),
                      textStyle: MaterialStateProperty.all(TextStyle(fontSize: 16)),
                    ),
                    onPressed: controller.onLogin,
                    child: Text('login_label'.tr),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }


}