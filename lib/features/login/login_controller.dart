import 'dart:typed_data';

import 'package:aes_cryptor/core/ui/status/status_controller.dart';
import 'package:aes_cryptor/core/data/contact.dart';
import 'package:aes_cryptor/core/utils/utils.dart';
import 'package:aes_cryptor/features/contact_list/contact_list_view.dart';
import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_controller.dart';
import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_view.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

class LoginController extends StatusController{


  final formKey = GlobalKey<FormState>();
  final TextEditingController passwordController = TextEditingController();
  //internal state
  final Uint8List correctHashMasterKey = Hive.box('login').get('correctMasterKeyHash');
  final Uint8List salt = Hive.box('login').get('salt');



  void onLogin() async{
    if (formKey.currentState!.validate()) {
      if(await _masterPwCorrect()){
        status.value = Status.loading;
        await _login();
        Get.find<CryptionFrameController>().isLoggedIn = true;
        Get.off(() => ContactListView());
      }else{
        status.value = Status.success;
        Get.snackbar('error_11_title'.tr, 'error_11_msg'.tr);
      }
    }
  }

  Future<void> _login() async{
    final masterKey = await Utils.pbkdf2(passwordController.text, iv: salt);
    await Hive.openBox<Contact>('contacts',encryptionCipher: HiveAesCipher(masterKey));
  }

  Future<bool> _masterPwCorrect() async{
    final userInputMasterKey =  await Utils.pbkdf2(passwordController.text, iv: salt);
    final userInputHMacMasterKey = await Utils.getHash(userInputMasterKey);
    return ListEquality().equals(userInputHMacMasterKey, correctHashMasterKey);
  }

  String? validatePw(String? text) => Utils.validatePw(text);


  @override
  void onClose() {
    passwordController.dispose();
    super.onClose();
  }


}