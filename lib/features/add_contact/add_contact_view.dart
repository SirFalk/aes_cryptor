import 'package:aes_cryptor/core/ui/default/default_app_bar.dart';
import 'package:aes_cryptor/core/ui/default/default_constaints.dart';
import 'package:aes_cryptor/core/ui/status/status_widget.dart';
import 'package:aes_cryptor/features/add_contact/add_contact_controller.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'add_contact_controller.dart';

class AddContactView extends GetView<AddContactController> {
  final AddContactController controller = Get.put(AddContactController());

  @override
  Widget build(BuildContext context) {
    return StatusWidget(
      controller,
      onSuccess: _page(),
    );
  }

  Widget _page() {
    final ICON_PADDING = 8.0;

    return Scaffold(
      appBar: DefaultAppBar('add_contact_title'.tr),
      body: DefaultConstraints(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Form(
              key: controller.formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      controller: controller.nameController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'name_label'.tr,
                      ),
                      validator: controller.validateName,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Obx(() => TextFormField(
                        keyboardType: TextInputType.text,
                        controller: controller.passwordController,
                        obscureText: controller.obscurePw.value,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'password_label'.tr,
                          suffixIcon: IconButton(
                            icon: Icon(
                              controller.obscurePw.value ? Icons.visibility : Icons.visibility_off,
                            ),
                            onPressed: controller.toggleVisiblity,
                          ),
                        ),
                        validator: controller.validatePw,
                      )),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(icon: Icon(Icons.loop), onPressed: controller.onGeneratePassword),
                      IconButton(icon: Icon(Icons.copy), onPressed: controller.onCopyPassword),
                      IconButton(icon: Icon(Icons.paste), onPressed: controller.onPastePassword),
                      IconButton(icon: Icon(Icons.clear), onPressed: controller.onClearPassword),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          minimumSize: MaterialStateProperty.all(Size.fromHeight(50.0)),
                          textStyle: MaterialStateProperty.all(TextStyle(fontSize: 16)),
                        ),
                        onPressed: controller.onAddContact,
                        child: Text('add_contact_button'.tr),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
