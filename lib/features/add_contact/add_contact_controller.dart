import 'package:aes_cryptor/core/ui/status/status_controller.dart';
import 'package:aes_cryptor/core/data/contact.dart';
import 'package:aes_cryptor/core/utils/utils.dart';
import 'package:hive/hive.dart';

import 'package:aes_cryptor/features/contact_list/contact_list_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class AddContactController extends StatusController{
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  RxBool obscurePw = true.obs;
  final ContactListController _contactListController = Get.find();


  void toggleVisiblity() => obscurePw.value = !obscurePw.value;
  bool validateForm() => formKey.currentState!.validate();


  void onAddContact() async{
    if(formKey.currentState!.validate()){
      status.value = Status.loading;
      _contactListController.addContact(Contact(nameController.text, passwordController.text));
      Get.back();
    }
  }


  get validateName => Utils.validateNonEmpty;
  get validatePw => Utils.validateNonEmpty;

  void onGeneratePassword() {
    passwordController.text = Utils.generateSecurePassword(12);
    passwordController.selection = TextSelection.collapsed(offset: passwordController.text.length);
    obscurePw.value = false;
  }
  void onClearPassword() => passwordController.clear();
  void onCopyPassword() => Utils.copyToClipboard(passwordController);
  void onPastePassword() => Utils.pasteFromClipboard(passwordController);

  @override
  void onClose() {
    nameController.dispose();
    passwordController.dispose();
    super.onClose();
  }
}
