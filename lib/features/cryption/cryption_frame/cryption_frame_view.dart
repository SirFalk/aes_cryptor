import 'package:aes_cryptor/core/ui/default/default_constaints.dart';
import 'package:aes_cryptor/core/ui/responsive.dart';
import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_controller.dart';
import 'package:aes_cryptor/features/cryption/file_cryption/file_cryption_controller.dart';
import 'package:aes_cryptor/features/cryption/file_cryption/file_cryption_view.dart';
import 'package:aes_cryptor/features/cryption/text_cryption/text_cryption_controller.dart';
import 'package:aes_cryptor/features/cryption/text_cryption/text_cryption_view.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../text_cryption/text_cryption_view.dart';
import 'cryption_frame_controller.dart';

class CryptionFrameView extends GetView<CryptionFrameController> {
  late final CryptionFrameController controller;

  @override
  CryptionFrameView() {
    Get.put(TextCryptionController());
    Get.put(FileCryptionController());
    controller = Get.put(CryptionFrameController());
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: controller.onSettingsTap,
            icon: Icon(Icons.settings),
          ),
          title: Text(controller.selectedContact.value == null ? 'cryption_frame_no_password'.tr : controller.selectedContact.value!.name),
          actions: [
            IconButton(onPressed:controller.onSelectQuickPassword, icon: Icon(Icons.password)),
            IconButton(
              onPressed: controller.onSelectContact,
              icon: Icon(Icons.person),
            ),
          ],
          centerTitle: true,
        ),
        body: Responsive(
          smallDevice: DefaultConstraints(
            child: Center(
              child: IndexedStack(
                children: [TextCryptionView(), FileCryptionView()],
                index: controller.selectedWindowIndex.value,
              ),
            ),
          ),
          bigDevice: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                DefaultConstraints(child: TextCryptionView()),
                DefaultConstraints(child: FileCryptionView()),
              ],
            ),
          ),
        ),
        bottomNavigationBar: Responsive(
          smallDevice: _bottomNavigationBar(),
          bigDevice: SizedBox.shrink(),
        ),
      ),
    );
  }

  Widget _bottomNavigationBar() {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.text_snippet),
          label: 'text_label'.tr,
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.insert_drive_file_outlined),
          label: 'file_label'.tr,
        ),
      ],
      currentIndex: controller.selectedWindowIndex.value,
      onTap: controller.onChangeSelectedWindow,
    );
  }

  Widget QuickPassword() {
    return AlertDialog(
      title: Text('cryption_frame_quick_password_label'.tr),
      content: Container(
        constraints: BoxConstraints(
          maxWidth: 500,
        ),
        child: SingleChildScrollView(
            child: Column(
          children: [
            Form(
              key: controller.formKey,
              child: Obx(() => TextFormField(
                    keyboardType: TextInputType.text,
                    obscureText: controller.obscureQuickPassword.value,
                    controller: controller.quickPasswordController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'password_label'.tr,
                      suffixIcon: IconButton(
                        icon: Icon(
                          controller.obscureQuickPassword.value ? Icons.visibility : Icons.visibility_off,
                        ),
                        onPressed: controller.onToggleQuickPasswordVisiblity,
                      ),
                    ),
                    validator: controller.validatePw,
                  )),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(icon: Icon(Icons.loop), onPressed: controller.onGeneratePassword),
                IconButton(icon: Icon(Icons.copy), onPressed: controller.onCopyPassword),
                IconButton(icon: Icon(Icons.paste), onPressed: controller.onPastePassword),
                IconButton(icon: Icon(Icons.clear), onPressed: controller.onClearPassword),
              ],
            ),
          ],
        )),
      ),
      actions: <Widget>[
        TextButton(
          child: Text('cancel_label'.tr),
          onPressed: controller.onCancelQuickPassword,
        ),
        TextButton(
          child: Text('confirm_label'.tr),
          onPressed: controller.onConfirmQuickPassword,
        ),
      ],
    );
  }
}
