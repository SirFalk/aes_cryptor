import 'package:aes_cryptor/core/data/contact.dart';
import 'package:aes_cryptor/core/utils/utils.dart';
import 'package:aes_cryptor/features/contact_list/contact_list_view.dart';
import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_view.dart';
import 'package:aes_cryptor/features/cryption/file_cryption/file_cryption_controller.dart';
import 'package:aes_cryptor/features/cryption/text_cryption/text_cryption_controller.dart';
import 'package:aes_cryptor/features/login/login_view.dart';
import 'package:aes_cryptor/features/register/register_view.dart';
import 'package:aes_cryptor/features/settings/settings_view.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:hive/hive.dart';

class CryptionFrameController extends GetxController {
  //late important here for lazy calling Get.find, because else we have circular dependencies
  late Rx<Contact?> selectedContact = Rx<Contact?>(null);
  late RxBool modeIsEncryptDesktop = textCryptionController.modeIsEncrypt;
  late RxBool modeIsEncryptMobile = textCryptionController.modeIsEncrypt;

  final textCryptionController = Get.find<TextCryptionController>();
  final fileCryptionController = Get.find<FileCryptionController>();
  final quickPasswordController = TextEditingController();
  RxInt selectedWindowIndex = 0.obs;
  bool isLoggedIn = false;
  RxBool obscureQuickPassword = true.obs;
  final formKey = GlobalKey<FormState>();

  //Keys for Intro
  final settingsKey = GlobalKey();

  void onChangeSelectedWindow(int index) {
    final actions = [textCryptionController.modeIsEncrypt, fileCryptionController.modeIsEncrypt];
    modeIsEncryptMobile = actions[index];
    selectedWindowIndex.value = index;
  }

  void onSelectContact() {
    if (isLoggedIn) {
      Get.to(() => ContactListView());
    } else {
      final bool loginExists = Hive.box('login').containsKey('salt') && Hive.box('login').containsKey('correctMasterKeyHash');
      if (loginExists)
        Get.to(() => LoginView());
      else
        Get.to(() => RegisterView());
    }
  }

  void onSelectQuickPassword() {
    //if the user changes the quick password but doesn't save it, this will
    // restore the selected quick password
    if (selectedContact.value != null)
      quickPasswordController.text = selectedContact.value!.password;

    Get.dialog(CryptionFrameView().QuickPassword());
  }

  void onSettingsTap() => Get.to(() => SettingsView());

  get validatePw => Utils.validateNonEmpty;

  void onCancelQuickPassword() => Get.back();


  void onConfirmQuickPassword() {
    if (formKey.currentState!.validate()) {
      selectedContact.value = Contact('cryption_frame_quick_password_label'.tr, quickPasswordController.text);
      Get.back();
    }
  }

  void onToggleQuickPasswordVisiblity() => obscureQuickPassword.toggle();

  void onGeneratePassword() {
    quickPasswordController.text = Utils.generateSecurePassword(12);
    quickPasswordController.selection = TextSelection.collapsed(offset: quickPasswordController.text.length);
    obscureQuickPassword.value = false;
  }

  void onClearPassword() => quickPasswordController.clear();

  void onCopyPassword() => Utils.copyToClipboard(quickPasswordController);

  void onPastePassword() => Utils.pasteFromClipboard(quickPasswordController);
}
