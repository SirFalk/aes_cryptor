import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:aes_cryptor/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_controller.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class TextCryptionController extends GetxController with GetSingleTickerProviderStateMixin{
  //controller
  final scrollController = ScrollController();
  final inputTextController = TextEditingController();
  final outputTextController = TextEditingController();
  late final animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 300));

  //late is important here! It will lead to laziness, which is
  //important because of the circular dependencies between CryptionFrameController and this
  late final contact = Get.find<CryptionFrameController>().selectedContact;

  RxBool modeIsEncrypt = true.obs;
  RxBool isLoading = false.obs;
  final leftText = 'Klartext'.obs;
  final rightText = 'Geheimtext'.obs;

  //ensures only clear cipherTextField when clearText actually changes
  String oldClearText = '';


  TextCryptionController(){
    modeIsEncrypt.listen((x) {x ?  animationController.reverse() : animationController.forward();});
  }


  void onSwitchMode() => modeIsEncrypt.toggle();

  void onCopyFromInput() => Utils.copyToClipboard(inputTextController);
  void onCopyFromOutput() => Utils.copyToClipboard(outputTextController);

  void onClearInput() {
    inputTextController.clear();
    outputTextController.clear();
    FocusManager.instance.primaryFocus?.unfocus();
    modeIsEncrypt.value = true;
  }
  void onClearOutput() => outputTextController.clear();

  void onPasteInput() async{
    await Utils.pasteFromClipboard(inputTextController);
    final base64Regex = RegExp('^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?\$');
    bool isBase64 = base64Regex.hasMatch(inputTextController.text);
    modeIsEncrypt.value = isBase64 ? false : true;
    if(isBase64) outputTextController.clear();
    FocusManager.instance.primaryFocus?.unfocus();
  }


  void onClearTextChange(String newText) {
    //if clause fixes a bug on Android where "onChange"-Event would also be triggered by pressing
    //any of the 3 Android control buttons
    if(newText != oldClearText){
      outputTextController.clear();
      oldClearText = newText;
    }
  }

  void onCryption() async {
    FocusManager.instance.primaryFocus?.unfocus();
    //check App state valid
    if (contact.value == null) {
      Get.snackbar('error_5_title'.tr, 'error_5_msg'.tr);
      return;
    }

    if(modeIsEncrypt.value)
      await _encryptText();
    else
      await _decryptText();

  }

  Future<void> _encryptText({EncryptionMethod em = EncryptionMethod.AES_CBC_PKCS7_HMAC_SHA256}) async{
    if(contact.value == null){
      Get.snackbar('error_5_title'.tr, 'error_5_msg'.tr);
      return;
    }
    if (inputTextController.text.isEmpty) {
      Get.snackbar('error_16_title'.tr, 'error_16_msg_1'.tr);
      return;
    }

    final Uint8List key = await Utils.pbkdf2(contact.value!.password);

    //prepare header
    final Uint8List magic = Uint8List.fromList(ascii.encode('SECENCRYPT'));
    final Uint8List headerVersionNumber = Uint8List(2)..buffer.asUint16List()[0] = 0;
    final Uint8List encryptionMethod = Uint8List(2)..buffer.asUint16List()[0] = em.index;
    final Uint8List isFileFlag = Uint8List.fromList([0]);
    final selectedBytes = Uint8List.fromList(utf8.encode(inputTextController.text));
    final Uint8List? messagePayload = await Utils.crypt(key, em, selectedBytes, true, runInParallel: false);
    if(messagePayload == null) return;



    //We only support File sizes of 2^32-1
    if(messagePayload.length > 4294967295){
      Get.snackbar('error_6_title'.tr, 'error_6_msg'.tr);
      return;
    }

    //Uint64 operations are not supported on web, so we are using float (double) which works almost the same in Dart
    //as int when there is no decimals aka 2.0 = 2
    final Uint8List payloadLength = Uint8List(4)..buffer.asInt32List()[0] = messagePayload.lengthInBytes;
    final BytesBuilder result = BytesBuilder()
      ..add(magic)
      ..add(headerVersionNumber)
      ..add(encryptionMethod)
      ..add(isFileFlag)
      ..add(payloadLength)
      ..add(messagePayload);

    final bigHMAC = await Utils.getHMAC(result.toBytes(), contact.value!.password);
    result.add(bigHMAC);

    outputTextController.text = base64.encode(result.toBytes());
  }

  Future<void> _decryptText() async {
    if (inputTextController.text.isEmpty) {
      Get.snackbar('error_16_title'.tr, 'error_16_msg_2'.tr);
      return;
    }
    final Uint8List? data;
    try{
      data = base64.decode(inputTextController.text.trim());
    }catch (e){
      Get.snackbar('error_17_title'.tr,'error_17_msg'.tr);
      return;
    }

    final split = await Utils.validateAndDecryptMessage(data, contact.value ,false);
    if (split == null) return;

    String? decoded;
    try{
      decoded = utf8.decode(split.message);
    }catch (e){
      Get.snackbar('error_22_title'.tr, 'error_22_msg'.tr);
      return;
    }
    outputTextController.text = decoded;
  }

  @override
  void onClose() {
    // TODO: implement onClose
    animationController.dispose();
    scrollController.dispose();
    inputTextController.dispose();
    outputTextController.dispose();
    super.onClose();
  }



  void onSwitchToDecrypt() => modeIsEncrypt.value = false;
  void onSwitchToEncrypt() => modeIsEncrypt.value = true;
}

