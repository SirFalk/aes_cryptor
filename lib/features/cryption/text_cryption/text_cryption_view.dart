import 'package:aes_cryptor/core/ui/default/default_button.dart';
import 'package:aes_cryptor/core/ui/default/default_constaints.dart';
import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'text_cryption_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TextCryptionView extends GetView<TextCryptionController> {
  final controller = Get.find<TextCryptionController>();

  //constants
  double BORDERWIDTH = 2.0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: DefaultConstraints(
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Obx(() => Container(
                            height: 50.0,
                            decoration: BoxDecoration(
                              color: controller.modeIsEncrypt.value ? context.theme.primaryColor : context.theme.canvasColor,
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(4.0),
                                topLeft: Radius.circular(4.0),
                              ),
                            ),
                            child: TextButton(
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  'plaintext_label'.tr,
                                  style: controller.modeIsEncrypt.value ? TextStyle(color: Colors.white) : TextStyle(),
                                ),
                              ),
                              onPressed: controller.onSwitchToEncrypt,
                            ),
                          )),
                    ),
                    Expanded(
                      flex: 1,
                      child: AnimatedContainer(
                        height: 50.0,
                        duration: Duration(milliseconds: 500),
                        child: RotationTransition(
                          turns: Tween(
                            begin: 0.0,
                            end: 0.5,
                          ).animate(
                            CurvedAnimation(
                              parent: controller.animationController,
                              curve: Curves.easeInOutCubic,
                            ),
                          ),
                          child: IconButton(
                            onPressed: controller.onSwitchMode,
                            icon: Icon(Icons.arrow_forward),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Obx(() => Container(
                            height: 50.0,
                            decoration: BoxDecoration(
                              color: controller.modeIsEncrypt.value ? context.theme.canvasColor : context.theme.primaryColor,
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(4.0),
                                topLeft: Radius.circular(4.0),
                              ),
                            ),
                            child: TextButton(
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  'ciphertext_label'.tr,
                                  style: controller.modeIsEncrypt.value ? TextStyle() : TextStyle(color: Colors.white),
                                ),
                              ),
                              onPressed: controller.onSwitchToDecrypt,
                            ),
                          )),
                    ),
                  ],
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: TextField(
                          keyboardType: TextInputType.multiline,
                          onChanged: controller.onClearTextChange,
                          controller: controller.inputTextController,
                          maxLines: 999,
                          decoration: InputDecoration(
                            hintText: 'text_cryption_cleartext_hint'.tr,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: context.theme.primaryColor,
                                width: BORDERWIDTH,
                              ),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(4.0),
                                bottomRight: Radius.circular(4.0),
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: context.theme.primaryColor,
                                width: BORDERWIDTH,
                              ),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(4.0),
                                bottomRight: Radius.circular(4.0),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Padding(
                              padding: EdgeInsets.only(top: 20.0),
                              child: Container(
                                height: 50.0,
                                decoration: BoxDecoration(
                                  color: Color.fromARGB(255, 33, 33, 33),
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(4.0),
                                    topLeft: Radius.circular(4.0),
                                  ),
                                ),
                                child: TextButton(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      'output_label'.tr,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                  onPressed: null,
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                IconButton(icon: Icon(Icons.copy), onPressed: controller.onCopyFromInput),
                                IconButton(
                                  icon: Icon(Icons.paste),
                                  onPressed: controller.onPasteInput,
                                ),
                                IconButton(
                                  icon: Icon(Icons.clear),
                                  onPressed: controller.onClearInput,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Obx(
                          () => TextField(
                            readOnly: true,
                            keyboardType: TextInputType.multiline,
                            maxLines: 999,
                            controller: controller.outputTextController,
                            decoration: InputDecoration(
                              hintText: controller.modeIsEncrypt.value ? 'text_cryption_ciphertext_hint1'.tr : 'text_cryption_ciphertext_hint2'.tr,
                              hintMaxLines: 2,
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 33, 33, 33),
                                  width: BORDERWIDTH,
                                ),
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(4.0),
                                  bottomRight: Radius.circular(4.0),
                                  topRight: Radius.circular(4.0),
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 33, 33, 33),
                                  width: BORDERWIDTH,
                                ),
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(4.0),
                                  bottomRight: Radius.circular(4.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            icon: Icon(Icons.copy),
                            onPressed: controller.onCopyFromOutput,
                          ),
                          IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: controller.onClearOutput,
                          ),
                        ],
                      ),
                      Obx(() => DefaultButton(
                            text: controller.modeIsEncrypt.value ? 'encrypt_label'.tr : 'decrypt_label'.tr,
                            onPressed: controller.onCryption,
                          )),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
