import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;
import 'package:path_provider/path_provider.dart';
import 'dart:math';
import 'dart:typed_data';
import 'package:aes_cryptor/core/data/file_data_split.dart';
import 'package:aes_cryptor/core/utils/utils.dart';
import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_controller.dart';
import 'package:collection/collection.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:share_plus/share_plus.dart';
import 'package:universal_html/html.dart' as html;

import 'file_cryption_view.dart';

class FileCryptionController extends GetxController {
  //late is important here! It will lead to laziness, which is
  //important because of the circular dependencies between CryptionFrameController and this
  late final contact = Get.find<CryptionFrameController>().selectedContact;

  final scrollController = ScrollController();

  final selectedFileName = RxnString(null);
  Uint8List? selectedFileBytes;
  RxBool modeIsEncrypt = true.obs;
  RxBool isLoading = false.obs;
  RxBool doEncryptFileName = true.obs;

  late StreamSubscription _intentDataStreamSubscription;

  String buttonLabel() {
    String result = modeIsEncrypt.value ? 'encrypt_label'.tr : 'decrypt_label'.tr;
    result += ' & ';
    result += kIsWeb
        ? 'download_label'.tr
        : GetPlatform.isMobile
            ? 'share_label'.tr
            : 'save_label'.tr;
    return result;
  }

  //for detecting share intent on mobile (using receiveShareIntent package)
  void onInit() {
    super.onInit();
    //Start share intent on mobile only
    if (GetPlatform.isMobile) {
      // For sharing files coming from outside the app while the app is in the memory
      _intentDataStreamSubscription = ReceiveSharingIntent.getMediaStream().listen((List<SharedMediaFile> value) {
        onReceiveFileShareIntent(value);
      }, onError: onReceiveFileShareIntentError);
      // For sharing files coming from outside the app while the app is closed
      ReceiveSharingIntent.getInitialMedia().then((List<SharedMediaFile> value) {
        onReceiveFileShareIntent(value);
      }, onError: onReceiveFileShareIntentError);
    }
  }

  //whenever something is shared execute this
  void onReceiveFileShareIntent(List<SharedMediaFile> value) async {
    if (value.length != 0) {
      Get.find<CryptionFrameController>().selectedWindowIndex.value = 1;
      final sharedMediaFile = value[0];
      String path = sharedMediaFile.path;
      //fixes a bug where the file path would sometimes include a random "file://" at the beginning
      path = path.replaceFirst('file://', '');
      final io.File file;
      if (await io.File(path).exists()) {
        file = io.File(path);
      } else {
        Get.snackbar('error_1_title'.tr, 'error_1_msg'.tr);
        return;
      }

      //ask user for abort when using large file
      final fileSize = await file.length();
      if (fileSize > 80000000) {
        final abort = await Get.dialog(FileCryptionView().fileAppearsLarge());
        if (abort) return;
      }

      selectedFileBytes = file.readAsBytesSync();

      selectedFileName.value = sharedMediaFile.path.split('/').last;
      if (selectedFileBytes != null) {
        modeIsEncrypt.value = !(ListEquality().equals(selectedFileBytes?.sublist(0, 10), ascii.encode('SECENCRYPT')));
      }
    }
  }

  void onReceiveFileShareIntentError(err) {
    Get.snackbar('error_2_title'.tr, err.toString());
  }

  //lets you select a file for decryption
  void onSelectFile() async {
    if (GetPlatform.isMobile) await FilePicker.platform.clearTemporaryFiles();
    final FilePickerResult? result;
    try {
      result = await FilePicker.platform.pickFiles(
        onFileLoading: (f) {
          if (f == FilePickerStatus.picking) {
            isLoading.value = true;
          } else {
            isLoading.value = false;
          }
        },
      );
    } catch (e) {
      e.printError();
      return;
    }

    //if user canceled picker, result will be null. If that is the case, return
    if (result == null) return;

    String name = result.names[0]!;

    //ask user to abort if file appears large
    if (result.files.single.size > 80000000 || kIsWeb && result.files.single.size > 5000000) {
      final abort = await Get.dialog(FileCryptionView().fileAppearsLarge());
      if (abort) return;
    }

    if (kIsWeb) {
      selectedFileBytes = result.files.single.bytes;
      selectedFileName.value = name;
    } else {
      String path = result.files.single.path!;
      final file = io.File(path);
      selectedFileBytes = file.readAsBytesSync();
      selectedFileName.value = name;
    }

    if (selectedFileBytes!.length < 10) {
      modeIsEncrypt.value == true;
    } else {
      modeIsEncrypt.value = !(ListEquality().equals(selectedFileBytes?.sublist(0, 10), ascii.encode('SECENCRYPT')));
    }
  }

  void onCryptFile() async {
    //check App state valid
    if (contact.value == null) {
      Get.snackbar('error_5_title'.tr, 'error_5_msg'.tr);
      return;
    }
    if (selectedFileBytes == null || selectedFileName.value == null) {
      Get.snackbar('error_3_title'.tr, 'error_3_msg'.tr);
      return;
    }
    if (selectedFileBytes!.isEmpty) {
      Get.snackbar('error_4_title'.tr, 'error_4_msg'.tr);
      return;
    }
    if (isLoading.value) {
      Get.closeAllSnackbars();
      Get.snackbar(
        'file_cryption_currently_loading_title'.tr,
        'file_cryption_currently_loading_body'.tr,
      );
      return;
    }

    modeIsEncrypt.value ? await _encryptAndSaveFile() : await _decryptAndSaveFile();

  }

  void onToggleDoEncryptFileName(bool value) => doEncryptFileName.value = value;

  Future<void> _encryptAndSaveFile({EncryptionMethod em = EncryptionMethod.AES_CBC_PKCS7_HMAC_SHA256}) async {
    final Uint8List key = await Utils.pbkdf2(contact.value!.password);

    //prepare header
    final Uint8List magic = Uint8List.fromList(ascii.encode('SECENCRYPT'));
    final Uint8List headerVersionNumber = Uint8List(2)..buffer.asUint16List()[0] = 0;
    final Uint8List encryptionMethod = Uint8List(2)..buffer.asUint16List()[0] = em.index;
    final Uint8List isFileFlag = Uint8List.fromList([1]);

    //prepare file specific header
    final Uint8List fileNameIsEncryptedFlag = Uint8List.fromList(doEncryptFileName.value ? [1] : [0]);

    Uint8List fileName = Uint8List.fromList(utf8.encode(selectedFileName.value!));

    if (doEncryptFileName.value) {
      Uint8List? fileNamePayload = await Utils.crypt(key, em, fileName, true);
      if (fileNamePayload == null) return;
      fileName = fileNamePayload;
    }
    final Uint8List fileNameLength = Uint8List(2)..buffer.asUint16List()[0] = fileName.length;

    //actual encryption
    final Uint8List? messagePayload;
    final unencryptedPayload = Uint8List.fromList(selectedFileBytes!);

    isLoading.value = true;
    messagePayload = await Utils.crypt(key, em, unencryptedPayload, true);
    isLoading.value = false;

    if (messagePayload == null) return;

    //Javascript only supports ints -2^53 <= x <= 2^53
    //if higher it will also show 2^53, so we support max size of (2^53)-1
    if (messagePayload.length > pow(2, 53) - 1) {
      Get.snackbar('error_6_title'.tr, 'error_6_msg'.tr);
      return;
    }

    //Uint64 operations are not supported on web, so we are using float (double) which works almost the same in Dart
    //as int when there is no decimals aka 2.0 = 2
    final Uint8List messagePayloadLength = Uint8List(4)..buffer.asUint32List()[0] = messagePayload.lengthInBytes;

    //stick it all together
    final BytesBuilder result = BytesBuilder()
      ..add(magic)
      ..add(headerVersionNumber)
      ..add(encryptionMethod)
      ..add(isFileFlag)
      ..add(fileNameIsEncryptedFlag)
      ..add(fileNameLength)
      ..add(fileName)
      ..add(messagePayloadLength)
      ..add(messagePayload);

    final bigHMAC = await Utils.getHMAC(result.toBytes(), contact.value!.password);
    result.add(bigHMAC);

    //save file
    final now = DateTime.now().toString().replaceAll(new RegExp(r'[^0-9]'), '');
    final saveAsFileName = doEncryptFileName.value ? 'Encrypted$now.secn' : '$selectedFileName.secn';

    await _saveFile(result.toBytes(), saveAsFileName, true);
  }

  Future<void> _decryptAndSaveFile() async {
    final Uint8List data = selectedFileBytes!;
    FileDataSplit? split;

    if (kIsWeb) {
      split = await Utils.validateAndDecryptMessage(data, contact.value, true) as FileDataSplit?;
    } else {
      isLoading.value = true;
      split = await Utils.validateAndDecryptMessage(data, contact.value, true) as FileDataSplit?;
      isLoading.value = false;
    }
    if (split == null) return;

    await _saveFile(split.message, split.fileName, false);
  }

  //saves file on different platforms
  Future<void> _saveFile(Uint8List data, String fileName, bool encrypt) async {
    if (kIsWeb) {
      final content = base64.encode(data);
      html.AnchorElement(href: 'data:application/octet-stream;base64,$content')
        ..setAttribute('download', fileName)
        ..click();
    } else {
      if (GetPlatform.isMobile) {
        //save file first to allow it to be shared
        final String appDirPath = (await getApplicationDocumentsDirectory()).path;
        final subDir = encrypt ? 'encryptedFiles' : 'decryptedFiles';
        final path = '$appDirPath/$subDir/$fileName';
        io.File saveFile = await io.File(path).create(recursive: true);
        await saveFile.writeAsBytes(data);
        Share.shareFiles([path]);
      } else {
        //save with file browser dialog
        String extension = fileName.split('.').last;
        extension = extension == fileName ? '' : extension;
        String? path = await FilePicker.platform.saveFile(
          fileName: fileName,
          dialogTitle: 'file_cryption_save_dialog_title'.tr,
          type: FileType.custom,
          allowedExtensions: [extension],
        );
        if (path == null) return;

        //This fixes a bug where if you f.e. saved as "example.extension" it would give you the path
        // "...\example\extension" or "...\example\.extension" or "...\extension" when you replace the name
        //there are even more bugs if the file extension exceeds 5 characters: it will sometimes give you
        // "...\example\sion"
        path = path.replaceAll(r'\.' + extension, '');
        path = path.replaceAll(r'\' + extension, '');
        path = path.replaceAll(r'.' + extension, '');
        path = path + '.' + extension;

        io.File encryptedFile = await io.File(path).create();
        await encryptedFile.writeAsBytes(data);
      }
    }
  }

  @override
  void onClose() {
    // TODO: implement onClose
    _intentDataStreamSubscription.cancel();
    scrollController.dispose();
    super.onClose();
  }

  void onAbortFileLoad() => Get.back(result: true);

  void onConfirmFileLoad() => Get.back(result: false);
}
