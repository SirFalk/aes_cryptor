import 'package:aes_cryptor/core/ui/default/default_button.dart';
import 'package:aes_cryptor/core/ui/default/default_constaints.dart';
import 'package:aes_cryptor/features/cryption/file_cryption/file_cryption_controller.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FileCryptionView extends GetView<FileCryptionController> {
  final controller = Get.find<FileCryptionController>();

  @override
  Widget build(BuildContext context) {
    return DefaultConstraints(
      child: Padding(
        padding: EdgeInsets.all(20.0),
        child: SingleChildScrollView(
            controller: controller.scrollController,
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DefaultButton(text: 'file_cryption_select_file'.tr, onPressed: controller.onSelectFile),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Obx(() => Text(controller.selectedFileName.value ?? 'file_cryption_no_file'.tr)),
            ),
            Obx(() => SwitchListTile(
              title: Text('file_cryption_encrypt_file_name'.tr),
              value: controller.doEncryptFileName.value,
              onChanged: controller.modeIsEncrypt.value ? controller.onToggleDoEncryptFileName : null,
            )),
            Obx(() => DefaultButton(
                text: (controller.buttonLabel()),
                onPressed: controller.onCryptFile
            )),
            Obx(() => Padding(
              padding: EdgeInsets.all(8.0),
              child: Visibility(
                    child: CircularProgressIndicator(),
                    maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: controller.isLoading.value,
                  ),
            )),

          ],
        )),
      ),
    );
  }

  Widget fileAppearsLarge() {
    return AlertDialog(
      title: Text('file_cryption_file_appears_large_dialog_title'.tr),
      content: Container(
        constraints: BoxConstraints(
          maxWidth: 500,
        ),
        child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 16.0),
                  child: Text(
                    'file_cryption_file_appears_large_dialog_body'.tr,
                    style: TextStyle(fontSize: 12.0),
                  ),
                ),
              ],
            )),
      ),
      actions: <Widget>[
        TextButton(
          child: Text('file_cryption_file_appears_large_dialog_abort'.tr),
          onPressed: controller.onAbortFileLoad,
        ),
        TextButton(
          child: Text('file_cryption_file_appears_large_dialog_continue'.tr),
          onPressed: controller.onConfirmFileLoad,
        ),
      ],
    );
  }


}


