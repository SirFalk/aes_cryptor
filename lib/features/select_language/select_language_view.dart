import 'package:aes_cryptor/core/ui/default/default_app_bar.dart';
import 'package:aes_cryptor/features/select_language/select_language_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/ui/default/default_constaints.dart';

class SelectLanguageView extends GetView<SelectLanguageController> {
  final SelectLanguageController controller = Get.put(SelectLanguageController());

  @override
  Widget build(BuildContext context) {
    return Obx(
          () => Center(
        child: Scaffold(
          appBar: DefaultAppBar('languages_sub_page_title'.tr),
          body: DefaultConstraints(
            child: Column(children: [
              ListTile(
                title: Text('Deutsch'),
                trailing: controller.languageCode == 'de' ? Icon(Icons.check, color: Colors.blue) : Icon(null),
                onTap: () => controller.onLanguageTap('de'),
              ),
              ListTile(
                title: Text('English'),
                trailing: controller.languageCode == 'en' ? Icon(Icons.check, color: Colors.blue) : Icon(null),
                onTap: () => controller.onLanguageTap('en'),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}