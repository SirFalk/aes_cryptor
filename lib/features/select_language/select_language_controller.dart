import 'package:aes_cryptor/features/settings/settings_controller.dart';
import 'package:get/get.dart';

class SelectLanguageController extends GetxController{
  final SettingsController settingsController = Get.find();

  get languageCode => settingsController.languageCode;
  get onLanguageTap=> settingsController.onChangeLanguageCode;


}