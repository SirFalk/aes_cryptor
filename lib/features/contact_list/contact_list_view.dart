import 'package:aes_cryptor/core/ui/default/default_app_bar.dart';
import 'package:aes_cryptor/core/ui/default/default_constaints.dart';
import 'package:aes_cryptor/core/ui/status/status_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'contact_list_controller.dart';

class ContactListView extends GetView<ContactListController> {
  final controller = Get.put(ContactListController());

  @override
  Widget build(BuildContext context) {
    return StatusWidget(
      controller,
      onSuccess: Obx(() => _page()),
    );
  }

  Widget _page() {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      appBar: DefaultAppBar('contact_list_title'.tr),
      body: DefaultConstraints(
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 10.0, bottom: 80.0, right: 10.0),
          child: Center(
            child: ListView.builder(
              itemCount: controller.contactList.length,
              itemBuilder: (context, index) {
                return Card(
                  child: ListTile(
                    onTap: () => controller.onContactTap(controller.contactList[index]),
                    title: Text(controller.contactList[index].name),
                    trailing: IconButton(
                      onPressed: () => controller.onContactDetailsTap(index),
                      icon: Icon(Icons.settings),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
      floatingActionButton: _addContactButton(),
    );
  }

  Widget _addContactButton() {
    return Container(
      height: 75.0,
      width: 75.0,
      child: FittedBox(
        alignment: Alignment.center,
        child: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () => controller.onNewContactTap(),
        ),
      ),
    );
  }
}
