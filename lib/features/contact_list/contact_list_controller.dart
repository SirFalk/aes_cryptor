import 'package:aes_cryptor/core/data/contact.dart';
import 'package:aes_cryptor/core/ui/status/status_controller.dart';
import 'package:aes_cryptor/features/add_contact/add_contact_view.dart';
import 'package:aes_cryptor/features/contact_settings/contact_settings_view.dart';
import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_controller.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

class ContactListController extends StatusController{

  final Rx<Box<Contact>> _box = Hive.box<Contact>('contacts').obs;


  List<Contact> get contactList => _box.value.values.toList();

  Contact contactAt(int index) => contactList[index];

  void addContact(Contact contact) => _box.update((val) {val!.add(contact);});
  void updateContact(int index, Contact contact) => _box.update((val) {val!.putAt(index,contact);});
  void deleteContact(int index) => _box.update((val) {val!.deleteAt(index);});


  void onContactTap(Contact contact) {Get.find<CryptionFrameController>().selectedContact.value = contact; Get.back();}
  void onContactDetailsTap(int index) => Get.to(() => ContactSettingsView(index));
  void onNewContactTap() => Get.to(() => AddContactView());

}


