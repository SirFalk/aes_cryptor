import 'package:aes_cryptor/features/settings/settings_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:introduction_screen/introduction_screen.dart';

import '../intro/intro_view.dart';
import '../select_language/select_language_view.dart';

class SettingsController extends GetxController{
  final _box = Hive.box('settings').obs;

  bool get darkMode => _box.value.get('darkMode');
  String get languageCode => _box.value.get('languageCode');

  onRedoIntro() => Get.off(() => IntroductionView());

  onToggleDarkMode(bool darkMode) async{
    _box.update((val) {val!.put('darkMode', darkMode); });
    await Get.forceAppUpdate();
  }
  onLanguageSubMenuTap() => Get.to(() => SelectLanguageView());
  onChangeLanguageCode(String languageCode) async {
    _box.update((val) {val!.put('languageCode', languageCode); });
    await Get.updateLocale(Locale(languageCode));
  }
}