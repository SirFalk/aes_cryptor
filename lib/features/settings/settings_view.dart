import 'package:aes_cryptor/core/ui/default/default_app_bar.dart';
import 'package:aes_cryptor/core/ui/default/default_constaints.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'settings_controller.dart';

class SettingsView extends GetView<SettingsController> {
  final controller = Get.put(SettingsController());

  @override
  Widget build(_) {
    return Obx(
        () => Scaffold(
          appBar: DefaultAppBar('settings_title'.tr),
          body: DefaultConstraints(
            child: Column(
              children: [
                ListTile(
                  title: Text('language_label'.tr),
                  subtitle: Text('settings_current_language'.tr),
                  leading: Icon(Icons.language),
                  onTap: controller.onLanguageSubMenuTap,
                ),
                SwitchListTile(
                  title: Text('dark_mode_label'.tr),
                  secondary: const Icon(Icons.wb_sunny_outlined),
                  value: controller.darkMode,
                  onChanged: controller.onToggleDarkMode,
                ),
                ListTile(
                  title: Text('settings_redo_tutorial'.tr),
                  leading: Icon(Icons.replay),
                  onTap: controller.onRedoIntro,
                ),
                ListTile(
                  title: Text('settings_more_info'.tr),
                  leading: Icon(Icons.info),
                  onTap: () => showAboutDialog(
                    context: Get.context!,
                    applicationName: 'SecureEncrypt',
                    applicationVersion: '1.0.0',
                    applicationIcon: SizedBox(
                      child: Image.asset('assets/icon.png'),
                      width: 40,
                      height: 40,
                    ),
                    applicationLegalese: 'Creator: Falk Müller\nSpecial thanks to: Bernhard Esslinger and Nils Kopal\nwww.cryptool.org\nAll Rights Reserved.'
                  ),
                ),
              ],
            ),
          ),
        ),
    );
  }

}


