import 'package:aes_cryptor/core/ui/default/default_button.dart';
import 'package:aes_cryptor/core/ui/default/default_constaints.dart';
import 'package:aes_cryptor/core/ui/status/status_widget.dart';
import 'package:flutter_pw_validator/Resource/Strings.dart';
import 'package:flutter_pw_validator/flutter_pw_validator.dart';
import 'package:aes_cryptor/core/ui/default/default_loading_widget.dart';

import 'package:flutter/material.dart';
import 'package:flutter_pw_validator/flutter_pw_validator.dart';
import 'package:get/get.dart';

import '../../core/utils/utils.dart';
import 'register_controller.dart';

class RegisterView extends GetView<RegisterController> {
  final controller = Get.put(RegisterController());

  @override
  Widget build(BuildContext context) {
    return StatusWidget(controller, onSuccess: _page());
  }

  Widget _page() {
    return Scaffold(
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Get.theme.iconTheme.color,
        ),
      ),
      body: DefaultConstraints(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: controller.masterPasswordController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'master_password_label'.tr,
                      suffixIcon: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween, // added line
                        mainAxisSize: MainAxisSize.min, // added line
                          children: [
                        IconButton(
                          icon: Icon(
                            Icons.loop,
                          ),
                          onPressed: controller.onGeneratePassword,
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.close,
                          ),
                          onPressed: controller.onClearPassword,
                        ),
                      ],),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new FlutterPwValidator(
                    controller: controller.masterPasswordController,
                    minLength: 12,
                    normalCharCount: 1,
                    uppercaseCharCount: 1,
                    numericCharCount: 1,
                    specialCharCount: 1,
                    width: 400,
                    height: 150,
                    onSuccess: controller.onPwValidatorSuccess,
                    onFail: controller.onPwValidatorFail,
                    strings: GlobalStrings(),
                  ),
                ),
                DefaultButton(
                  text: 'register_button'.tr,
                  onPressed: controller.onRegister,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('register_notice'.tr),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
