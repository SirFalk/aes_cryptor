import 'package:aes_cryptor/core/ui/status/status_controller.dart';
import 'package:aes_cryptor/core/data/contact.dart';
import 'package:aes_cryptor/core/utils/utils.dart';

import 'package:aes_cryptor/features/contact_list/contact_list_view.dart';
import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_controller.dart';
import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';


class RegisterController extends StatusController{

  //ui State
  final formKey = GlobalKey<FormState>();
  final masterPasswordController = TextEditingController();
  bool pwIsStrong = false;

  RxBool obscureMasterPassword = true.obs;


  String? validatePw(String? text) => Utils.validatePw(text);


  void onRegister() async{
    // Validate returns true if the form is valid, or false otherwise.
    if (pwIsStrong) {
      status.value = Status.loading;
      await _createNewLogin();
      Get.find<CryptionFrameController>().isLoggedIn = true;
      Get.off(() => ContactListView());
    }
  }

  //unencrypted box 'login' stores salt and hash(kdf((password, salt))
  Future<void> _createNewLogin() async{
    final salt = Utils.getSecureUint8List(32);
    final masterKey =  await Utils.pbkdf2(masterPasswordController.text,iv: salt);
    final hash = await Utils.getHash(masterKey);

    //put in login box
    final box = Hive.box('login');
    await box.put('salt', salt);
    await box.put('correctMasterKeyHash', hash);
    await Hive.openBox<Contact>('contacts', encryptionCipher: HiveAesCipher(masterKey));

  }

  void onToggleMasterPasswordVisiblity() => obscureMasterPassword.toggle();

  void onPwValidatorSuccess() => pwIsStrong = true;
  void onPwValidatorFail() => pwIsStrong = false;

  void onGeneratePassword() {
    masterPasswordController.text = Utils.generateSecurePassword(12);
    masterPasswordController.selection = TextSelection.collapsed(offset: masterPasswordController.text.length);
  }
  void onClearPassword() => masterPasswordController.clear();

  @override
  void onClose() {
    masterPasswordController.dispose();
    super.onClose();
  }
}