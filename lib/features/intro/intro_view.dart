import 'package:aes_cryptor/core/ui/default/default_app_bar.dart';
import 'package:aes_cryptor/core/ui/default/default_constaints.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';

import 'intro_controller.dart';

class IntroductionView extends GetView<IntroductionController> {
  final controller = Get.put(IntroductionController());

  Widget _buildImage(String assetName) {
    return Center(
      child: Image.asset('assets/intro/' + ('language_short_label'.tr) + '/$assetName'),
    );
  }

  PageDecoration getPageDecoration({double? titlePaddingTop}) {
    return PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700, color: Colors.black),
      bodyTextStyle: TextStyle(fontSize: 19.0, color: Colors.black),
      bodyPadding: EdgeInsets.only(bottom: 16.0),
      titlePadding: EdgeInsets.only(top: titlePaddingTop ?? 16.0, bottom: 24.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: IntroductionScreen(
        controlsPadding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 8.0),
        key: controller.introKey,
        globalBackgroundColor: Colors.white,
        pages: [
          PageViewModel(
            title: 'intro_page1_title'.tr,
            body: 'intro_page1_body'.tr,
            image: _buildImage('img1.png'),
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            title: 'intro_page2_title'.tr,
            body: 'intro_page2_body'.tr,
            image: _buildImage('img2.png'),
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            title: 'intro_page3_title'.tr,
            body: 'intro_page3_body'.tr,
            image: _buildImage('img3.png'),
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            title: 'intro_page4_title'.tr,
            body: 'intro_page4_body'.tr,
            image: _buildImage('img4.png'),
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            title: 'intro_page5_title'.tr,
            body: 'intro_page5_body'.tr,
            image: _buildImage('img5.png'),
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            title: 'intro_page6_title'.tr,
            body: 'intro_page6_body'.tr,
            image: _buildImage('img6.png'),
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            title: 'intro_page7_title'.tr,
            bodyWidget: Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              child: Text.rich(
                TextSpan(
                  children: [
                    TextSpan(text: 'intro_page7_body1'.tr, style: TextStyle(color: Colors.black),),
                    WidgetSpan(child: Icon(Icons.password, size: 20, color: Colors.black),),
                    TextSpan(text: 'intro_page7_body2'.tr, style: TextStyle(color: Colors.black),),
                    WidgetSpan(child: Icon(Icons.person, size: 20, color: Colors.black)),
                    TextSpan(text: 'intro_page7_body3'.tr, style: TextStyle(color: Colors.black),),
                  ],
                ),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20.0),
              ),
            ),
            image: _buildImage('img7.png'),
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            title: 'intro_page8_title'.tr,
            body: 'intro_page8_body'.tr,
            image: _buildImage('img8.png'),
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            title: 'intro_page9_title'.tr,
            body: 'intro_page9_body'.tr,
            image: _buildImage('img10.png'),
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            title: 'intro_page10_list_of_icons'.tr,
            decoration: getPageDecoration(titlePaddingTop: 24.0),
            bodyWidget: Table(
              columnWidths: const <int, TableColumnWidth>{
                0: FlexColumnWidth(1.0),
                1: FlexColumnWidth(2.0),
              },
              children: [
                ...<dynamic>[
                  [Icons.copy, 'intro_page10_icon_copy'.tr],
                  [Icons.paste, 'intro_page10_icon_paste'.tr],
                  [Icons.loop, 'intro_page10_icon_generate'.tr],
                  [Icons.clear, 'intro_page10_icon_clear'.tr],
                  [Icons.password, 'intro_page10_icon_quick_pw'.tr],
                  [Icons.person, 'intro_page10_icon_pw_list'.tr],
                  [Icons.settings, 'intro_page10_icon_pw_settings'.tr],
                ].map((e) => TableRow(children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: Icon(e[0],color: Colors.black,),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: Text(
                          e[1],
                          style: TextStyle(fontSize: 19.0, color: Colors.black),
                        ),
                      ),
                    ])),
              ],
            ),
          ),
        ],
        isTopSafeArea: true,
        isBottomSafeArea: true,
        showSkipButton: true,
        onSkip: controller.onIntroEnd,
        onDone: controller.onIntroEnd,
        skipOrBackFlex: 1,
        nextFlex: 1,
        dotsFlex: 4,
        next: const Icon(Icons.arrow_forward),
        skip: Text('skip_label'.tr),
        done: Text('done_label'.tr),
        curve: Curves.easeInOut,
        controlsMargin: const EdgeInsets.all(16),
        dotsDecorator: const DotsDecorator(
          activeSize: Size(22.0, 10.0),
          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
        ),
        dotsContainerDecorator: const ShapeDecoration(
          color: Colors.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
        ),
      ),
    );
  }
}
