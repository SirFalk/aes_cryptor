import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:introduction_screen/introduction_screen.dart';

class IntroductionController extends GetxController {
  final introKey = GlobalKey<IntroductionScreenState>();

  void onIntroEnd() {
    Hive.box('settings').put('showIntro', false);
    Get.off(() => CryptionFrameView());
  }

}