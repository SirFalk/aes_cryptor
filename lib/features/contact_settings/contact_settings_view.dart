import 'package:aes_cryptor/core/ui/default/default_app_bar.dart';
import 'package:aes_cryptor/core/ui/default/default_constaints.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'contact_settings_controller.dart';

class ContactSettingsView extends GetView<ContactSettingsController> {
  late final ContactSettingsController controller;

  ContactSettingsView(index) {
    controller = Get.put(ContactSettingsController(index));
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => _page());
  }

  Widget _page() {
    return Scaffold(
      appBar: AppBar(
        title: Text('contact_settings_title'.tr),
        centerTitle: true,
    ),
      body: DefaultConstraints(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Form(
              key: controller.formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      controller: controller.nameController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'name_label'.tr,
                      ),
                      validator: controller.validateName,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      controller: controller.passwordController,
                      obscureText: controller.obscurePw.value,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'password_label'.tr,
                        suffixIcon: IconButton(
                          icon: Icon(
                            controller.obscurePw.value ? Icons.visibility : Icons.visibility_off,
                          ),
                          onPressed: controller.toggleVisiblity,
                        ),
                      ),
                      validator: controller.validatePw,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(icon: Icon(Icons.loop), onPressed: controller.onGeneratePassword),
                      IconButton(icon: Icon(Icons.copy), onPressed: controller.onCopyPassword),
                      IconButton(icon: Icon(Icons.paste), onPressed: controller.onPastePassword),
                      IconButton(icon: Icon(Icons.clear), onPressed: controller.onClearPassword),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        minimumSize: MaterialStateProperty.all(Size.fromHeight(50.0)),
                        textStyle: MaterialStateProperty.all(TextStyle(fontSize: 16)),
                      ),
                      onPressed: controller.onUpdateContact,
                      child: Text('contact_settings_update_button'.tr),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        minimumSize: MaterialStateProperty.all(Size.fromHeight(50.0)),
                        textStyle: MaterialStateProperty.all(TextStyle(fontSize: 16)),
                      ),
                      onPressed: controller.onDeleteContactTap,
                      child: Text('contact_settings_delete_button'.tr),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
