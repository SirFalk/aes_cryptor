import 'package:aes_cryptor/core/data/contact.dart';
import 'package:aes_cryptor/core/utils/utils.dart';
import 'package:aes_cryptor/features/contact_list/contact_list_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';




class ContactSettingsController extends GetxController with StateMixin<Contact>{

  late final TextEditingController nameController;
  late final TextEditingController passwordController;

  final ContactListController contactListController = Get.find();
  final formKey = GlobalKey<FormState>();
  final int contactIndex;
  RxBool obscurePw = true.obs;



  ContactSettingsController(this.contactIndex){
    final contact = Get.find<ContactListController>().contactAt(contactIndex);
    nameController = TextEditingController(text: contact.name);
    passwordController = TextEditingController(text: contact.password);
  }

  get validateName => Utils.validateNonEmpty;
  get validatePw => Utils.validateNonEmpty;


  void toggleVisiblity() => obscurePw.value = !obscurePw.value;
  void onGeneratePassword() {
    passwordController.text = Utils.generateSecurePassword(12);
    passwordController.selection = TextSelection.collapsed(offset: passwordController.text.length);
    obscurePw.value = false;
  }
  void onClearPassword() => passwordController.clear();
  void onCopyPassword() => Utils.copyToClipboard(passwordController);
  void onPastePassword() => Utils.pasteFromClipboard(passwordController);


  void onUpdateContact() {
    if (formKey.currentState!.validate()) {
      contactListController.updateContact(contactIndex, Contact(nameController.text, passwordController.text));
      Get.back();
    }
  }

  void onDeleteContactTap() {
    contactListController.deleteContact(contactIndex);
    Get.back();
  }

  @override
  void onClose() {
    nameController.dispose();
    passwordController.dispose();
    super.onClose();
  }

}
