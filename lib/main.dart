import 'package:aes_cryptor/core/translations/messages.dart';
import 'package:aes_cryptor/features/cryption/cryption_frame/cryption_frame_view.dart';
import 'package:aes_cryptor/features/intro/intro_view.dart';
import 'package:cryptography/cryptography.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter/material.dart';
import 'package:convert/convert.dart';

import 'core/data/contact.dart';





void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter('hive');
  Hive.registerAdapter(ContactAdapter());

  //for resetting the app
  await Hive.deleteBoxFromDisk('login');
  await Hive.deleteBoxFromDisk('settings');
  await Hive.deleteBoxFromDisk('contacts');

  if (!(await Hive.boxExists('settings'))) {
    await Hive.openBox('settings')
      ..put('darkMode', false)
      ..put('languageCode', Get.deviceLocale!.languageCode)
      ..put('showIntro',true);
  }

  await Hive.openBox('login');
  await Hive.openBox('settings');


  //keep App in portrait mode all the time
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  runApp(new AESCryptor());
}

class AESCryptor extends StatelessWidget {
  // This widget is the root of the application.
  @override
  Widget build(BuildContext context) {
    //every Android app needs MaterialApp as root
    //to access all features of the get package, we use GetMaterialApp
    return GetMaterialApp(
      //internationalize
      translations: Messages(),
      debugShowCheckedModeBanner: false,
      theme: Hive.box('settings').get('darkMode') ? ThemeData.dark() : ThemeData.light(),
      locale: Locale(Hive.box('settings').get('languageCode')),
      fallbackLocale: Locale('en'),
      home: Hive.box('settings').get('showIntro') ? IntroductionView() : CryptionFrameView(),
    );
  }
}