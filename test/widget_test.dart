// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text_cryption, and verify that the values of widget properties are correct.

import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:aes_cryptor/core/utils/utils.dart';
import 'package:collection/collection.dart';
import 'package:convert/convert.dart';
import 'package:pointycastle/digests/sha256.dart';
import 'package:pointycastle/key_derivators/pbkdf2.dart';
import 'package:pointycastle/macs/hmac.dart';
import 'package:test/test.dart';
import 'package:cryptography/cryptography.dart';
import 'package:pointycastle/pointycastle.dart';
import 'package:universal_html/html.dart';

void main() {

  test('Verify pointycastle and cryptography algorithms for AES_CBC_PKCS7 and HMAC are equal', () async{

    int max = 10000;
    for(int i = 0; i < max; i++){
      Random random = new Random();
      int randomNumber = random.nextInt(10000) + 1;
      final message = Utils.getSecureUint8List(randomNumber);

      // AES-CBC with 256 bit keys and HMAC-SHA256 authentication.
      final algorithm = AesCbc.with256bits(
        macAlgorithm: Hmac.sha256(),
      );
      final key = Utils.getSecureUint8List(32);
      final iv = Utils.getSecureUint8List(16);

      // Encrypt
      final secretBox = await algorithm.encrypt(
        message,
        secretKey: SecretKey(key),
        nonce: iv,
      );
      final Uint8List actualCipherText = Uint8List.fromList(secretBox.cipherText);
      final Uint8List actualMAC = Uint8List.fromList(secretBox.mac.bytes);

      final paddedBlockCipher = PaddedBlockCipher('AES/CBC/PKCS7');
      final parametersWithIV = ParametersWithIV<KeyParameter>(KeyParameter(key), iv);
      final paddedBlockCipherParameters = PaddedBlockCipherParameters(parametersWithIV, null);
      paddedBlockCipher.init(true, paddedBlockCipherParameters);
      final expectedCipherText = paddedBlockCipher.process(message);


        final hmac = HMac(SHA256Digest(), 64) // for HMAC SHA-256, block length must be 64
          ..init(KeyParameter(key));
        final expectedMAC = hmac.process(Uint8List.fromList(expectedCipherText));


      expect(ListEquality().equals(actualCipherText, expectedCipherText), true);
      expect(ListEquality().equals(actualMAC, expectedMAC), true);

    }

  });

  test('Verify pointycastle and cryptography algorithms for PBKDF2 are equal', () async{

    int max = 1000;
    for(int i = 0; i < max; i++){
      //generate random string
      var r = Random();
      const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
      final pw = List.generate(60, (index) => _chars[r.nextInt(_chars.length)]).join();

      final salt = Utils.getSecureUint8List(16);
      final derivator = PBKDF2KeyDerivator(HMac(SHA256Digest(), 64))
        ..init(Pbkdf2Parameters(salt, 10000, 32));
      final expected = derivator.process(Uint8List.fromList(utf8.encode(pw)));
      final actual = await Utils.pbkdf2(pw,iv: salt);

      expect(ListEquality().equals(actual, expected), true);
    }

  });

  test('Verify pointycastle and cryptography algorithms for SHA-256 are equal', () async{

    int max = 10000;
    for(int i = 0; i < max; i++){
      Random random = new Random();
      int randomNumber = random.nextInt(10000) + 1;
      final message = Utils.getSecureUint8List(randomNumber);

      //pointycastle
      final expected = SHA256Digest().process(message);
      //cryptography
      final actual = Uint8List.fromList((await Sha256().hash(message)).bytes);

      expect(ListEquality().equals(actual, expected), true);
    }

  });




}


